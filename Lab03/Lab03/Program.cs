﻿using System;
using System.IO;
using AES;

namespace Lab03
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                switch (args[0])
                {
                    case "-key":
                        WriteKey(args[1]);
                        break;
                    case "-encrypt":
                        Encrypt(args[1], args[2], args[3]);
                        break;
                    case "-decrypt":
                        Decrypt(args[1], args[2], args[3]);
                        break;
                    default:
                        Console.WriteLine("Unknown command");
                        break;
                }
            }
            catch (Exception)
            {
                Console.WriteLine("incorrect input data, must be -command keypath input output");
            }
        }

        static byte[] GetKey(Stream stream)
        {
            var res = new byte[16];
            stream.Read(res, 0, 16);
            return res;
        }
        private static void Decrypt(string keyPath, string inputPath, string outputPath)
        {
            using (var keyStream = new FileStream(keyPath, FileMode.Open, FileAccess.Read))
            using (var inputStream = new FileStream(inputPath, FileMode.Open, FileAccess.Read))
            using (var outputStream = new FileStream(outputPath, FileMode.Create, FileAccess.Write))
            {
                var key = GetKey(keyStream);
                var aes = new AesCipher(key);
                aes.Decrypt(inputStream, outputStream);
            }
        }

        private static void Encrypt(string keyPath, string inputPath, string cipherPath)
        {
            using (var keyStream = new FileStream(keyPath, FileMode.Open, FileAccess.Read))
            using (var inputStream = new FileStream(inputPath, FileMode.Open, FileAccess.Read))
            using (var outputStream = new FileStream(cipherPath, FileMode.Create, FileAccess.Write))
            {
                var key = GetKey(keyStream);
                var aes = new AesCipher(key);
                aes.Encrypt(inputStream, outputStream);
            }
        }

        private static void WriteKey(string keyPath)
        {
            using (var stream = new FileStream(keyPath, FileMode.Create))
            {
                var key = AesCipher.CreateKey();
                stream.Write(key, 0, key.Length);
                stream.Flush();
            }
        }
    }
}
