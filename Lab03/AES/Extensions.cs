﻿namespace AES
{
    public static class Extensions
    {
        public static byte[] GetColumn(this byte[][] table, int column)
        {
            var result = new byte[table.GetLength(0)];
            for (int i = 0; i < result.Length; ++i)
                result[i] = table[i][column];
            return result;
        }

        public static void XorBy(this byte[][] table, byte[][] input, int column)
        {
            int rowCount = table.Length;
            int columnCount = table[0].Length;
            for(int i = 0; i < rowCount; ++i)
                for (int j = 0; j < columnCount; ++j)
                    table[i][j] ^= input[i][column + j];
        }
    }
}
