﻿using System;

namespace AES
{
    public static class Multiplicator
    {
        public static byte[] MultiplyMatrix(byte[,] matrixA, byte[] vectorB)
        {
            int aRows = matrixA.GetLength(0); int aCols = matrixA.GetLength(1);
            int bRows = vectorB.Length;
            if (aCols != bRows)
                throw new Exception("Non-conformable matrices in MatrixProduct");
            byte[] result = new byte[aRows];
            for (int i = 0; i < aRows; ++i) // each row of A
                for (int k = 0; k < aCols; ++k)
                    result[i] ^= vectorB[k].GaluaMult(matrixA[i,k]);
            return result;
        }

        public static byte GaluaMult(this byte input, byte by)
        {
            switch (by)
            {
                case 1:
                    return input;
                case 2:
                    return By02(input);
                case 3:
                    return By03(input);
                case 9:
                    return By09(input);
                case 0x0b:
                    return By0b(input);
                case 0x0d:
                    return By0d(input);
                case 0x0e:
                    return By0e(input);
                default:
                    throw new ArgumentException();
            }
        }

        private static byte By02(byte num)
        {
            byte res;
            if (num < 0x80)
            {
                res = (byte)(num << 1);
            }
            else
            {
                res = (byte)((num << 1) ^ 0x1b);
            }
            return (byte) (res%0x100);
        }

        private static byte By03(byte num)
        {
            return (byte) (By02(num) ^ num);
        }

        private static byte By09(byte num)
        {
            return (byte) (By02(By02(By02(num))) ^ num);
        }

        private static byte By0b(byte num)
        {
            return (byte)(By02(By02(By02(num))) ^ By02(num)^ num);
        }

        private static byte By0d(byte num)
        {
            return (byte)(By02(By02(By02(num))) ^ By02(By02(num)) ^ num);
        }

        private static byte By0e(byte num)
        {
            return (byte)(By02(By02(By02(num))) ^ By02(By02(num)) ^ By02(num));
        }
    }
}
