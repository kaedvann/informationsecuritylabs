﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AES
{
    public class AesCipher
    {
        private const int Nb = 4;
        private const int Nk = 4;
        private const int Nr = 10;
        private readonly byte[] _key;
        internal readonly byte[][] _keySchedule;

        public AesCipher(byte[] key)
        {
            if (key.Length != 16)
                throw new ArgumentException();
            _key = key;
            _keySchedule = CreateKeySchedule(key);
        }

        public static byte[] CreateKey()
        {
            var r = new Random();
            var result = new byte[16];
            r.NextBytes(result);
            return result;
        }

        private static byte[][] CreateKeySchedule(byte[] key)
        {
            var keySchedule = new byte[4][];
            for (int i = 0; i < 4; ++i)
                keySchedule[i] = new byte[Nb*(Nr + 1)];
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    keySchedule[i][j] = key[i + 4*j];
            for (int j = Nk; j < Nb*(Nr + 1); ++j)
            {
                if (j%Nk == 0)
                {
                    byte[] shifted = ShiftColumnUp(keySchedule, j - 1);
                    shifted = shifted.Select(Tables.SubByte).ToArray();
                    XorColumns(shifted, Tables.GetRconColumn(j/Nk - 1), keySchedule, j);
                    XorColumns(keySchedule.GetColumn(j), keySchedule.GetColumn(j - Nk), keySchedule, j);
                }
                else
                {
                    XorColumns(keySchedule.GetColumn(j - 1), keySchedule.GetColumn(j - Nk), keySchedule, j);
                }
            }
            return keySchedule;
        }

        private static void XorColumns(IList<byte> op1, IList<byte> op2, byte[][] output, int outindex)
        {
            for (int i = 0; i < op1.Count; ++i)
                output[i][outindex] = (byte) (op1[i] ^ op2[i]);
        }


        internal static byte[] ShiftColumnUp(byte[][] table, int column)
        {
            int rowCount = table.GetLength(0);
            var temp = new byte[rowCount];
            for (int i = 0; i < rowCount - 1; ++i)
            {
                temp[i] = table[i + 1][column];
            }
            temp[rowCount - 1] = table[0][column];
            return temp;
        }

        internal byte[] EncryptBlock(byte[] block)
        {
            byte[][] state = CreateState(block);
            state.XorBy(_keySchedule, 0);
            for (int i = 1; i <= Nr; ++i)
                EncryptionRound(state, i);
            var result = new byte[block.Length];
            for (int i = 0; i < result.Length; ++i)
                result[i] = state[i%4][i/4];
            return result;
        }

        internal byte[] DecryptBlock(byte[] block)
        {
            byte[][] state = CreateState(block);
            state.XorBy(_keySchedule, Nr*4);
            for (int i = Nr - 1; i >= 0; --i)
                DecryptionRound(state, i);
            var result = new byte[block.Length];
            for (int i = 0; i < result.Length; ++i)
                result[i] = state[i%4][i/4];
            return result;
        }

        private void DecryptionRound(byte[][] state, int number)
        {
            RightShiftRows(state);
            SubBytes(state, Tables.InvSubByte);
            state.XorBy(_keySchedule, number*4);
            if (number != 0)
                MixColumns(state, Tables.InvMixColumnsTable);
        }

        private void RightShiftRows(byte[][] state)
        {
            for (int i = 0; i < state.Length; ++i)
                RightShiftRow(state[i], i);
        }

        private void RightShiftRow(byte[] bytes, int count)
        {
            var temp = new byte[bytes.Length];
            Array.Copy(bytes, temp, temp.Length);
            for (int i = 0; i < temp.Length; ++i)
            {
                int index = i - count;
                if (index < 0)
                    index += temp.Length;
                bytes[i] = temp[index];
            }
        }

        private void EncryptionRound(byte[][] state, int number)
        {
            SubBytes(state, Tables.SubByte);
            LeftShiftRows(state);
            if (number != Nr)
                MixColumns(state, Tables.MixColumnsTable);
            state.XorBy(_keySchedule, number*4);
        }

        private void LeftShiftRows(byte[][] state)
        {
            for (int i = 0; i < state.Length; ++i)
                LeftShiftRow(state[i], i);
        }

        internal static void LeftShiftRow(byte[] bytes, int count)
        {
            var temp = new byte[bytes.Length];
            Array.Copy(bytes, temp, temp.Length);
            for (int i = 0; i < temp.Length; ++i)
            {
                int index = i + count;
                if (index >= temp.Length)
                    index -= temp.Length;
                bytes[i] = temp[index];
            }
        }

        private void SubBytes(byte[][] state, Func<byte, byte> subByte)
        {
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    state[i][j] = subByte(state[i][j]);
        }

        internal static void MixColumns(byte[][] state, byte[,] table)
        {
            for (int i = 0; i < Nb; ++i)
            {
                byte[] mixed = Multiplicator.MultiplyMatrix(table, state.GetColumn(i));
                for (int j = 0; j < Nb; ++j)
                    state[j][i] = mixed[j];
            }
        }

        internal static byte[][] CreateState(byte[] block)
        {
            var state = new byte[4][];
            for (int i = 0; i < 4; ++i)
            {
                state[i] = new byte[4];
            }
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    state[i][j] = block[i + 4*j];
            return state;
        }

        public void Decrypt(Stream inputStream, Stream decipheredStream)
        {
            var buffer = new byte[16];
            int read = inputStream.Read(buffer, 0, 16);
            while (read == 16)
            {
                byte[] res = DecryptBlock(buffer);
                if (inputStream.ReadByte() != -1)
                {
                    inputStream.Seek(-1, SeekOrigin.Current);
                    decipheredStream.Write(res, 0, 16);
                    read = inputStream.Read(buffer, 0, 16);
                }
                else
                {
                    var truncated = res.TakeWhile((b, i) => !(b == 0x80 && res.Skip(i+1).All(e => e==0))).ToArray();
                    decipheredStream.Write(truncated, 0, truncated.Length);
                    read = 0;
                }

            }
            decipheredStream.Flush();
        }

        public void Encrypt(Stream inputStream, Stream cipherStream)
        {
            var buffer = new byte[16];
            int read = inputStream.Read(buffer, 0, 16);
            while (read == 16)
            {
                byte[] res = EncryptBlock(buffer);
                cipherStream.Write(res, 0, 16);
                read = inputStream.Read(buffer, 0, 16);
            }
            buffer[read] = 0x80;
            for (int i = read + 1; i < 16; ++i)
                buffer[i] = 0;
            byte[] ciph = EncryptBlock(buffer);
            cipherStream.Write(ciph, 0, 16);
            cipherStream.Flush();
        }
    }
}