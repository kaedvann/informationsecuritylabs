﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using AES;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class AesTests
    {
        [Test]
        public void TestShiftingUp()
        {
            var tables = new[] {new byte[] {0, 1, 2}, new byte[] {3, 4, 5}, new byte[] {6, 7, 8}};
            var res = AesCipher.ShiftColumnUp(tables, 1);
            Assert.AreEqual(4, res[0]);
            Assert.AreEqual(7, res[1]);
            Assert.AreEqual(1, res[2]);
        }

        [Test]
        public void TestCreate()
        {
            var key = new Byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
            var aes = new AesCipher(key);
            Assert.AreEqual(0xd6, aes._keySchedule[0][4]);
        }

        [Test]
        public void TestMixColumns()
        {
            var data = new byte[]{0xdb, 0x13, 0x53, 0x45};

            var expected = new byte[] { 0x8e, 0x4d, 0xa1, 0xbc };
            var result = Multiplicator.MultiplyMatrix(Tables.MixColumnsTable, data);

            Assert.IsTrue(result.SequenceEqual(expected));
        }

        [TestCase(new byte[] { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff })]
        [TestCase(new byte[] { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 })]
        [TestCase(new byte[] { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0 })]
        [TestCase(new byte[] { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 })]
        public void TestSimmetricity(byte[] data)
        {
            var key = new Byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
            var aes = new AesCipher(key);
            var inputStream = new MemoryStream(data);
            var cipherStream = new MemoryStream();
            var result = new MemoryStream();
            aes.Encrypt(inputStream, cipherStream);
            cipherStream.Seek(0, SeekOrigin.Begin);
            aes.Decrypt(cipherStream, result);
            
            Assert.IsTrue(result.ToArray().SequenceEqual(data));
        }

        [Test]
        public void TestEncryptBlock()
        {
            var key = new Byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
            var aes = new AesCipher(key);
            var data = new byte[] { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff };
            var expected = new byte[] {0x69, 0xc4, 0xe0, 0xd8, 0x6a, 0x7b, 0x04, 0x30, 0xd8, 0xcd, 0xb7, 0x80, 0x70, 0xb4, 0xc5, 0x5a};
            var result = aes.EncryptBlock(data);
            Assert.IsTrue(result.SequenceEqual(expected));
        }

        [Test]
        public void TestLeftShift()
        {
            var bytes = new byte[] {0, 1, 2, 3, 4};
            AesCipher.LeftShiftRow(bytes, 2);
            Assert.IsTrue(bytes.SequenceEqual(new byte[]{2, 3, 4, 0, 1}));
        }
    }
}
