﻿using AES;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class TablesTests
    {
        [TestCase(0xe5, 0x2a)]
        [TestCase(0x7c, 01)]
        public void TestSingleByteSub(byte expected, byte input)
        {
            var res = Tables.SubByte(input);
            Assert.AreEqual(expected, res);
        }
    }
}
