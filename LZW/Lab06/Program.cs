﻿using System;
using System.IO;
using LZW;

namespace Lab06
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                switch (args[0])
                {
                    case "-encrypt":
                        Encrypt(args[1], args[2]);
                        break;
                    case "-decrypt":
                        Decrypt(args[1], args[2]);
                        break;
                    default:
                        Console.WriteLine("Unknown command");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("incorrect input data, must be -command input output, exception message:{0}", e.Message);
            }
        }


        private static void Decrypt(string inputPath, string outputPath)
        {
            using (var inputStream = new FileStream(inputPath, FileMode.Open, FileAccess.Read))
            using (var outputStream = new FileStream(outputPath, FileMode.Create, FileAccess.Write))
            {
                var compressor = new Compressor();
                compressor.Decrypt(inputStream, outputStream);
            }
        }

        private static void Encrypt(string inputPath, string cipherPath)
        {
            using (var inputStream = new FileStream(inputPath, FileMode.Open, FileAccess.Read))
            using (var outputStream = new FileStream(cipherPath, FileMode.Create, FileAccess.Write))
            {
                var compressor = new Compressor();
                compressor.Encrypt(inputStream, outputStream);
            }
        }
    }
}
