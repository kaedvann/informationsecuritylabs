﻿using System.IO;
using System.Linq;
using LZW;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class LzwTests
    {
        [TestCase(new byte[] { 0, 0,0,0,0,0,0,0,0,0,0,0 })]
        [TestCase(new byte[] { 0, 1, 2, 0, 1, 5, 6, 2, 3, 0, 1 })]
        [TestCase(new byte[] { 0, 1, 2, 3, 1, 5, 6, 2, 3, 0, 1 })]
        [TestCase(new byte[] { 0, 1, 2, 0, 1, 5, 6, 2, 3, 0, 1 })]
        [TestCase(new byte[] { 0, 1, 2, 0, 1, 2, 6, 2, 3, 0, 1, 10, 20, 11})]
        public void TestEncrypt(byte[] source)
        {
            var array = source;
            var input = new MemoryStream(array);
            var output = new MemoryStream();
            var lzw = new Compressor();
            lzw.Encrypt(input, output);
            var newResult = new MemoryStream();
            output.Seek(0, SeekOrigin.Begin);
            lzw.Decrypt(output, newResult);
            var resultArray = newResult.ToArray();
            Assert.IsTrue(array.SequenceEqual(resultArray));
        }

        [Test]
        public void TestBigArray()
        {
            var array = Enumerable.Range(11, 0xafff).Select(i => (byte)i).ToArray();
            var input = new MemoryStream(array);
            var output = new MemoryStream();
            var lzw = new Compressor();
            lzw.Encrypt(input, output);
            var newResult = new MemoryStream();
            output.Seek(0, SeekOrigin.Begin);
            lzw.Decrypt(output, newResult);
            var resultArray = newResult.ToArray();
            Assert.IsTrue(array.SequenceEqual(resultArray));
        }
    }

    
}
