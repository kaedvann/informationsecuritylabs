﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LZW;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class BitBufferTests
    {
        [Test]
        public void TestWriting()
        {
            var stream = new MemoryStream();
            var buffer = new BitBuffer(stream);
            var bits = Convert.ToInt32("101011011", 2);
            buffer.BlockSize = 9;
            buffer.WriteBits(bits);
            Assert.IsTrue(buffer.WrittenBits.SequenceEqual(new List<byte>{1,0,1,0,1,1,0,1,1}));
        }

        [Test]
        public void TestToByte()
        {
            var number = new List<byte> {1, 1, 0, 1, 0, 1, 1, 1};
            var result = BitBuffer.ToByte(number);
            Assert.AreEqual(Convert.ToByte("11010111", 2), result);
        }

        [Test]
        public void TestToInt()
        {
            var number = new List<byte> { 1, 1, 0, 1, 0, 1, 1, 1, 1 };
            var buf = new BitBuffer(new MemoryStream());
            buf.BlockSize = 9;
            var result = buf.ToInt(number);
            Assert.AreEqual(Convert.ToInt32("110101111", 2), result);
        }

        [Test]
        public void TestFlushing()
        {
            var stream = new MemoryStream();
            var buffer = new BitBuffer(stream);
            var bits = Convert.ToInt32("1010110111110000", 2);
            buffer.BlockSize = 16;
            buffer.WriteBits(bits);
            buffer.Flush();
            var resultArray = stream.ToArray();
            Assert.IsTrue(resultArray.SequenceEqual(new List<byte> {0xAD, 0xF0 }));
        }
        [Test]
        public void TestWriteAndRead()
        {
            var stream = new MemoryStream();
            var writeBuf = new BitBuffer(stream);
            var firstbits = Convert.ToInt32("1010110111110000", 2);
            writeBuf.BlockSize = 16;
            writeBuf.WriteBits(firstbits);
            var secondBits = Convert.ToInt32("101011011111", 2);
            writeBuf.BlockSize = 12;
            writeBuf.WriteBits(secondBits);
            writeBuf.Flush();
            var readBuf = new BitBuffer(stream);
            stream.Seek(0, SeekOrigin.Begin);
            readBuf.BlockSize = 16;
            Assert.AreEqual(firstbits, readBuf.ReadBits());
            readBuf.BlockSize = 12;
            Assert.AreEqual(secondBits, readBuf.ReadBits());
            Assert.AreEqual(-1, readBuf.ReadBits());
        }
    }
}
