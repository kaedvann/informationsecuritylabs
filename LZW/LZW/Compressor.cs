﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LZW
{
    public class Compressor
    {
        private const int MaxTableSize = 0x1fe;
        private const int NineBitsMaxSize = 0x1ff;
        private const int TenBitsMaxSize = 0x3ff;
        private const int ElevenBitsMaxSize = 0x7ff;
        
        public void Encrypt(Stream inputStream, Stream outputStream)
        {
            var table = new Dictionary<List<int>, int>(new ListComparer<int>());
            for (var i = 0; i < 256; i++)
                table.Add(new List<int>{i}, i);

            var buffer = new BitBuffer(outputStream) {BlockSize = 9};
            var w = new List<int>();

            int currentByte;
            while (( currentByte = inputStream.ReadByte()) != -1)
            {
                var wc = new List<int>(w) {currentByte};
                if (table.ContainsKey(wc))
                {
                    w = wc;
                }
                else
                {
                    // write w to output
                    buffer.WriteBits(table[w]);
                    // wc is a new sequence; add it to the dictionary
                    if (table.Count < MaxTableSize)
                    {
                        table.Add(wc, table.Count);
                        if (table.Count-1 == NineBitsMaxSize)
                            buffer.BlockSize = 10;
                        if (table.Count-1 == TenBitsMaxSize)
                            buffer.BlockSize = 11;
                        if (table.Count-1 == ElevenBitsMaxSize)
                            buffer.BlockSize = 12;

                    }
                    w = new List<int>{currentByte};
                }
            }
 
            // write remaining output if necessary
            if (w.Any())
                buffer.WriteBits(table[w]);
            buffer.Flush();
        }

        public void Decrypt(Stream compressed, Stream output)
        {
            var dictionary = new Dictionary<int, List<int>>();
            for (var i = 0; i < 256; i++)
                dictionary.Add(i, new List<int>{i});

            var buffer = new BitBuffer(compressed);
            buffer.BlockSize = 9;
            var w = dictionary[buffer.ReadBits()].ToList();
            output.WriteByte((byte)w[0]);
            int k;
            while ((k = buffer.ReadBits()) != -1)
            {
                var symbs = new List<int>();

                if (dictionary.ContainsKey(k))
                    symbs.AddRange(dictionary[k]);
                else if (k == dictionary.Count)
                {
                    symbs.AddRange(w);
                    symbs.Add(w[0]);
                }

                foreach (var symb in symbs)
                {
                    output.WriteByte((byte) symb);
                }
                // new sequence; add it to the dictionary
                if (dictionary.Count < MaxTableSize)
                {
                    dictionary.Add(dictionary.Count, new List<int>(w) {symbs[0]});
                    if (dictionary.Count == NineBitsMaxSize)
                        buffer.BlockSize = 10;
                    if (dictionary.Count == TenBitsMaxSize)
                        buffer.BlockSize = 11;
                    if (dictionary.Count == ElevenBitsMaxSize)
                        buffer.BlockSize = 12;
                }
                w = symbs;
            }
            output.Flush();
        }
    }
}
