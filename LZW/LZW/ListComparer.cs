﻿using System.Collections.Generic;
using System.Linq;

namespace LZW
{
    class ListComparer<T> : IEqualityComparer<List<T>>
    {
        public bool Equals(List<T> x, List<T> y)
        {
            return x.SequenceEqual(y);
        }

        public int GetHashCode(List<T> obj)
        {
            unchecked
            {
                return obj.Aggregate(19, (current, foo) => current*31 + foo.GetHashCode());
            }
        }
    }
}