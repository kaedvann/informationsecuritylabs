﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LZW
{
    public class BitBuffer: IDisposable
    {
        private readonly Stream _stream;

        private const int MaxBufferSize = 8096;

        private readonly List<byte> _writtenBits = new List<byte>();
        private readonly List<byte> _readBits = new List<byte>();

        public int BlockSize { get; set; }

        public List<byte> WrittenBits
        {
            get { return _writtenBits; }
        }

        public BitBuffer(Stream stream)
        {
            _stream = stream;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void WriteBits(int bits)
        {
            WrittenBits.AddRange(ToBytesSequence(bits));
            if (WrittenBits.Count > MaxBufferSize)
                WriteBlock();
        }

        public int ReadBits()
        {
            while (_readBits.Count < BlockSize)
            {
                if (!ReadMoreBits())
                    return -1;
            }
            var result = ToInt(_readBits.Take(BlockSize).ToList());
            _readBits.RemoveRange(0, BlockSize);
            return result;
        }

        private bool ReadMoreBits()
        {
            var res = _stream.ReadByte();
            if (res < 0)
            {
                return false;
            }
            _readBits.AddRange(new BitArray(new[] {(byte)res}).Cast<bool>().Select(b => (byte)(b ? 1:0)).Reverse());
            return true;
        }

        private void WriteBlock()
        {
            var totalBytes = new List<byte>();
            var currentBits = new List<byte>();
            foreach (var bit in _writtenBits)
            {
                currentBits.Add(bit);
                if (currentBits.Count == 8)
                {
                    totalBytes.Add(ToByte(currentBits));
                    currentBits.Clear();
                }
            }
            var array = totalBytes.ToArray();
            WrittenBits.RemoveRange(0, totalBytes.Count*8);
            _stream.Write(array, 0, array.Length);
        }

        public static byte ToByte(List<byte> currentBits)
        {
            if (currentBits.Count != 8)
                throw  new ArgumentException();
            byte result = 0;
            for (var i = 0; i < 8; ++i)
                result |= (byte)(currentBits[i] << (7 - i));
            return result;
        }

        public int ToInt(List<byte> currentBits)
        {
            if (currentBits.Count != BlockSize)
                throw new ArgumentException();
            var result = 0;
            for (var i = 0; i < BlockSize; ++i)
                result |= (currentBits[i] << (BlockSize - 1 - i));
            return result;
        }

        private IEnumerable<byte> ToBytesSequence(int bits)
        {
            var bitarray = new BitArray(new[] {bits});
            return bitarray.Cast<bool>().Select(b => (byte) (b ? 1 : 0)).Reverse().Skip(32 - BlockSize);
        }

        public void Flush()
        {
            while (_writtenBits.Count % 8 != 0)
            {
                _writtenBits.Add(0);
            }
            WriteBlock();
            _stream.Flush();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _stream.Dispose();
            }
        }
    }
}
