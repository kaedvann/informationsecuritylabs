﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace Lab04
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                switch (args[0])
                {
                    case "-key":
                        WriteKeys();
                        break;
                    case "-sign":
                        Sign(args[1], args[2], args[3]);
                        break;
                    case "-verify":
                        Verify(args[1], args[2], args[3]);
                        break;
                    default:
                        Console.WriteLine("Unknown command");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("incorrect input data, must be -command keypath input output");
            }
        }

        private static void Verify(string keyPath, string messagePath, string signaturePath)
        {
            using (var keyStream = new FileStream(keyPath, FileMode.Open, FileAccess.Read))
            using (var inputStream = new FileStream(messagePath, FileMode.Open, FileAccess.Read))
            using (var signatureStream = new FileStream(signaturePath, FileMode.Open, FileAccess.Read))
            {
                var rsa = new RSACryptoServiceProvider();
                var key = ReadFully(keyStream);
                rsa.ImportCspBlob(key);
                var result = rsa.VerifyData(ReadFully(inputStream), "SHA1", ReadFully(signatureStream));
                Console.WriteLine(result ? "Verification succeded" : "Verification failed");
            }
        }
        public static byte[] ReadFully(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        private static void Sign(string keyPath, string inputPath, string outputPath)
        {
            using (var keyStream = new FileStream(keyPath, FileMode.Open, FileAccess.Read))
            using (var inputStream = new FileStream(inputPath, FileMode.Open, FileAccess.Read))
            using (var outputStream = new FileStream(outputPath, FileMode.Create, FileAccess.Write))
            {
                var rsa = new RSACryptoServiceProvider();
                var key = ReadFully(keyStream);
                rsa.ImportCspBlob(key);
                var result = rsa.SignData(inputStream, "SHA1");
                outputStream.Write(result, 0, result.Length);
                outputStream.Flush();
            }
        }

        private static void WriteKeys()
        {
            var rsaprovider = new RSACryptoServiceProvider(2048);
            var privateKey = rsaprovider.ExportCspBlob(true);
            var publicKey = rsaprovider.ExportCspBlob(true);
            using (var stream = new FileStream("private", FileMode.Create))
            {
                stream.Write(privateKey, 0, privateKey.Length);
                stream.Flush();
            }
            using (var stream = new FileStream("public", FileMode.Create))
            {
                stream.Write(publicKey, 0, publicKey.Length);
                stream.Flush();
            }
        }
    }
}
