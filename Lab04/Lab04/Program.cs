﻿using System;
using System.Collections.Generic;
using System.IO;
using RSA;

namespace Lab04
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                switch (args[0])
                {
                    case "-key":
                        WriteKeys("private", "public");
                        break;
                    case "-encrypt":
                        Encrypt(args[1], args[2], args[3]);
                        break;
                    //case "-decrypt":
                    //    Decrypt(args[1], args[2], args[3]);
                    //    break;
                    default:
                        Console.WriteLine("Unknown command");
                        break;
                }
            }
            catch (Exception)
            {
                Console.WriteLine("incorrect input data, must be -command keypath input output");
            }
        }

        private static byte[] GetKey(Stream stream)
        {
            List<byte> res = new List<byte>();
            int read;
            while ( (read = stream.ReadByte()) != -1)
            {
                res.Add((byte) read);
            }
            return res.ToArray();
        }

        private static void Decrypt(string keyPath, string inputPath, string outputPath)
        {
            using (var keyStream = new FileStream(keyPath, FileMode.Open, FileAccess.Read))
            using (var inputStream = new FileStream(inputPath, FileMode.Open, FileAccess.Read))
            using (var outputStream = new FileStream(outputPath, FileMode.Create, FileAccess.Write))
            {
                //var key = GetKey(keyStream);
                //var aes = new AesCipher(key);
                //aes.Decrypt(inputStream, outputStream);
            }
        }

        private static void Encrypt(string keyPath, string inputPath, string cipherPath)
        {
            using (var keyStream = new FileStream(keyPath, FileMode.Open, FileAccess.Read))
            using (var inputStream = new FileStream(inputPath, FileMode.Open, FileAccess.Read))
            using (var outputStream = new FileStream(cipherPath, FileMode.Create, FileAccess.Write))
            {
                var key = Key.Deserialize(keyStream);
                RsaWorker.Transform(key, inputStream, outputStream);
            }
        }

        private static void WriteKeys(string privateKeyPath, string publicKeyPath)
        {

            using (var publicstream = new FileStream(publicKeyPath, FileMode.Create))
            using (var privatestream = new FileStream(privateKeyPath, FileMode.Create))
            {
                var keyPair = KeyGenerator.GenerateKeys();
                keyPair.PrivateKey.Serialize(privatestream);
                keyPair.PublicKey.Serialize(publicstream);
                publicstream.Flush();
                privatestream.Flush();
            }
        }
    }
}
