﻿using System;
using System.Numerics;

namespace RSA
{
    public class RsaKeyPair
    {
        public Key PrivateKey { get; set; }
        public Key PublicKey { get; set; }
    }

    public static class KeyGenerator
    {
        public static RsaKeyPair GenerateKeys()
        {
            var p = PrimeWorker.GetPrime();
            BigInteger q;
            do
            {
                q = PrimeWorker.GetPrime();
            }
            while ( q == p);
            var module = p*q;
            var eulerFunc = (p - 1)*(q - 1);
            var e = GetE(eulerFunc);
            var d = GetD(e, eulerFunc);
            return new RsaKeyPair { PrivateKey = new Key(){Exp = e, N = module} , PublicKey = new Key(){Exp = d, N = module}};
        }

        private static BigInteger GetE(BigInteger eulerFunc)
        {
            BigInteger e;
            do
            {
                e = PrimeWorker.Random(1, eulerFunc);
            }
            while (BigInteger.GreatestCommonDivisor(e, eulerFunc) != 1);
            return e;
        }

        private static BigInteger GetD(BigInteger e, BigInteger eulerFunc)
        {
            BigInteger t = 0,     newt = 1,    
            r = eulerFunc,     newr = e;    
            while (newr != 0)
            {
                var quotient = r/newr;
                var tempt = t;
                t = newt;
                newt = tempt - quotient*newt;

                var tempr = r;
                r = newr;
                newr = tempr - quotient*newr;
            }
            if ( r > 1)
                throw new InvalidOperationException();
            if (t < 0)
                t += eulerFunc;
            return t;
        }
    }

}