using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace RSA
{
    public class Key
    {
        public BigInteger Exp { get; set; }
        public BigInteger N { get; set; }

        public void Serialize(Stream stream)
        {
            var array = Exp.ToByteArray();
            var size = BitConverter.GetBytes(array.Length);
            stream.Write(size, 0, size.Length);
            stream.Write(array, 0, array.Length);
            array = N.ToByteArray();
            stream.Write(array, 0, array.Length);
            stream.Flush();
        }

        public static Key Deserialize(Stream stream)
        {
            var sizeArray = new byte[4];
            stream.Read(sizeArray, 0, 4);
            int size = BitConverter.ToInt32(sizeArray, 0);
            var exparray = new byte[size];
            stream.Read(exparray, 0, exparray.Length);
            var res = new List<byte>();
            int read;
            while ( (read = stream.ReadByte()) != -1)
            {
                res.Add((byte) read);
            }
            var narray = res.ToArray();
            return new Key(){Exp = new BigInteger(exparray), N = new BigInteger(narray)};
        }
    }
}