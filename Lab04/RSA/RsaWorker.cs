﻿using System;
using System.IO;
using System.Linq;
using System.Numerics;

namespace RSA
{
    public static class RsaWorker
    {
        public static void Transform(Key key, Stream input, Stream output)
        {
            var buffer = new byte[key.N.ToByteArray().Length];
            int read = input.Read(buffer, 0, buffer.Length);
            if (read > 0)
            {
                var message = new BigInteger(buffer.Take(read).ToArray());
                if (message > key.N)
                    throw new ArgumentException();
                var encrypted = BigInteger.ModPow(message, key.Exp, key.N);
                var encryptedArray = encrypted.ToByteArray();
                output.Write(encryptedArray, 0, encryptedArray.Length);
            }
            output.Flush();
        }
    }
}
