using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;

namespace RSA
{
    public static class PrimeWorker
    {
        private static bool IsPrime(BigInteger integer)
        {
            return MillerRabin(integer, 400);
        }

        private static bool MillerRabin(BigInteger n, int s)
        {
            var nMinusOne = BigInteger.Subtract(n, 1);

            for (var j = 1; j <= s; j++)
            {
                var a = Random(1, nMinusOne);

                if (Witness(a, n))
                {
                    return false;
                }
            }

            return true;
        }

        public static BigInteger Random(BigInteger min, BigInteger max)
        {
            var maxBytes = max.ToByteArray();
            var maxBits = new BitArray(maxBytes);
            var random = new Random(DateTime.Now.Millisecond);

            for (var i = 0; i < maxBits.Length; i++)
            {
                // Randomly set the bit
                var randomInt = random.Next();
                if ((randomInt % 2) == 0)
                {
                    // Reverse the bit
                    maxBits[i] = !maxBits[i];
                }
            }

            var result = new BigInteger();

            // Convert the bits back to a BigInteger
            for (var k = (maxBits.Count - 1); k >= 0; k--)
            {
                BigInteger bitValue = 0;

                if (maxBits[k])
                {
                    bitValue = BigInteger.Pow(2, k);
                }

                result = BigInteger.Add(result, bitValue);
            }

            // Generate the random number
            var randomBigInt = BigInteger.ModPow(result, 1, BigInteger.Add(max, min));
            return randomBigInt;
        }

        private static bool Witness(BigInteger a, BigInteger n)
        {
            var tAndU = GetTAndU(BigInteger.Subtract(n, 1));
            var t = tAndU.Key;
            var u = tAndU.Value;
            var x = new BigInteger[t + 1];

            x[0] = BigInteger.ModPow(a, u, n);

            for (var i = 1; i <= t; i++)
            {
                // x[i] = x[i-1]^2 mod n
                x[i] = BigInteger.ModPow(BigInteger.Multiply(x[i - 1], x[i - 1]), 1, n);
                var minus = BigInteger.Subtract(x[i - 1], BigInteger.Subtract(n, 1));

                if (x[i] == 1 && x[i - 1] != 1 && !minus.IsZero)
                {
                    return true;
                }
            }

            return !x[t].IsOne;
        }

        private static KeyValuePair<int, BigInteger> GetTAndU(BigInteger nMinusOne)
        {
            // Convert n - 1 to a byte array
            var nBytes = nMinusOne.ToByteArray();
            var bits = new BitArray(nBytes);
            var t = 0;
            var u = new BigInteger();

            var n = bits.Count - 1;
            var lastBit = bits[n];

            // Calculate t
            while (!lastBit)
            {
                t++;
                n--;
                lastBit = bits[n];
            }

            for (var k = ((bits.Count - 1) - t); k >= 0; k--)
            {
                BigInteger bitValue = 0;

                if (bits[k])
                {
                    bitValue = BigInteger.Pow(2, k);
                }

                u = BigInteger.Add(u, bitValue);
            }

            var tAndU = new KeyValuePair<int, BigInteger>(t, u);
            return tAndU;
        }

        public static BigInteger GetPrime()
        {
            var value = new byte[128];
            var rand = new Random();
            rand.NextBytes(value);
            var result = new BigInteger(value);
            if (result.IsEven)
                result++;
            if (result.Sign == -1)
                result *= -1;
            while (!IsPrime(result))
            {
                result += 2;
            }
            return result;
        }
    }
}