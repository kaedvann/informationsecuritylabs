﻿using NUnit.Framework;
using RSA;

namespace Tests
{
    [TestFixture]
    public class KeyGenerationTests
    {
        [Test]
        public void TestGettingPrime()
        {
            var prime = PrimeWorker.GetPrime();
            var result = prime.ToByteArray();
            Assert.IsTrue(result.Length == 128);
        }

        [Test]
        public void TestCreatingKeyPair()
        {
            var keyPair = KeyGenerator.GenerateKeys();
        }
    }
}
