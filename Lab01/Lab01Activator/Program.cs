﻿using System;
using System.IO;
using System.Net.NetworkInformation;

namespace Lab01Activator
{
    class Program
    {
        private static byte[] GetMacAddress()
        {
            const int minMacAddrLength = 6;
            byte[] macAddress = null;
            long maxSpeed = -1;

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                byte[] tempMac = nic.GetPhysicalAddress().GetAddressBytes();
                if (nic.Speed > maxSpeed &&
                    tempMac.Length >= minMacAddrLength)
                {
                    maxSpeed = nic.Speed;
                    macAddress = tempMac;
                }
            }

            return macAddress;
        }

        private static void Main()
        {
            var addr = GetMacAddress();
            Console.WriteLine("Patching...");
            using (var stream = File.Open("Lab01.exe", FileMode.Open, FileAccess.ReadWrite))
            {
                stream.Seek(0x2a8, SeekOrigin.Begin);
                stream.Write(addr, 0, addr.Length);
            }
            Console.WriteLine("Patched!");
        }
    }
}
