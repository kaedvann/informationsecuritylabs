﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;

namespace Lab01
{
    class Program
    {

        private static byte[] GetMacAddress()
        {
            const int minMacAddrLength = 6;
            byte[] macAddress = null;
            long maxSpeed = -1;

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                byte[] tempMac = nic.GetPhysicalAddress().GetAddressBytes();
                if (nic.Speed > maxSpeed &&
                    tempMac.Length >= minMacAddrLength)
                {
                    maxSpeed = nic.Speed;
                    macAddress = tempMac;
                }
            }
            return macAddress;
        }
        private static bool CheckRights()
        {
            byte[] test = {1,1,1,1,4,5};
            var actual = GetMacAddress();
            return test.SequenceEqual(actual);
        }

        private static void Main()
        {
            if (!CheckRights())
            {Console.WriteLine("You shall not pass!");
                Console.ReadLine();
                Thread.CurrentThread.Abort();
            }
            Console.WriteLine("Hello, I'm working");
            Console.ReadLine();
        }
    }
}
