﻿using System.IO;

namespace EnigmaEncryptor
{
    public interface IEncryptor
    {
        void Encrypt(Stream input, Stream output);
    }
}
