﻿using System.IO;

namespace EnigmaEncryptor
{
    public interface IDecryptor
    {
        void Decrypt(Stream input, Stream output);
    }
}