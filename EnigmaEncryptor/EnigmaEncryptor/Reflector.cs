﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EnigmaEncryptor
{
    internal sealed class Reflector : Wheel
    {
        private Reflector(IList<byte> table) : base(table)
        {
        }

        public static Reflector CreateReflector(IList<byte> table)
        {
            if (VerifyTable(table) && Wheel.VerifyTable(table))
                return new Reflector(table);
            throw new ArgumentException("Incorrect table for reflector");
        }

        private new static bool VerifyTable(IList<byte> table)
        {
            return table.All(b => table.IndexOf(b) == table[b]);
        }

        public override void Turn()
        {}
    }
}