﻿using System;
using System.Collections.Generic;

namespace EnigmaEncryptor
{
    public static class TableGenerator
    {
        private static Random Random = new Random();

        public static IList<byte> GenereteWheelTable()
        {
            var table = new List<byte>(256);
            while (table.Count != 256)
            {
                var newItem = (byte) Random.Next();
                if (!table.Contains(newItem))
                    table.Add(newItem);
            }
            return table;
        }

        public static IList<byte> GenerateReflectorTable()
        {
            var table = new List<byte>(256);
            for (int b = 128; b <= 255; ++b)
            {
                table.Add((byte)b);
            }
            for (byte b = 0; b < 128; ++b)
            {
                table.Add(b);
            }
            return table;
        }
    }
}
