﻿using System;
using System.Collections.Generic;

namespace EnigmaEncryptor
{
    internal class Wheel
    {
        private readonly ByteTable _table;

        protected Wheel(IList<byte> table)
        {
            _table = new ByteTable(table);
        }

        public void Reset()
        {
            _table.Offset = 0;
        }

        public static Wheel CreateWheel(IList<byte> table)
        {
            if (VerifyTable(table))
                return new Wheel(table);
            throw new ArgumentException("incorrect table");
        }

        protected static bool VerifyTable(IList<byte> table)
        {
            for (int b = 0; b <= byte.MaxValue; b++)
            {
                if (!table.Contains((byte)b))
                    return false;
            }
            return table.Count == 256;
        }

        public event EventHandler FullTurn;

        protected virtual void OnFullTurn()
        {
            var handler = FullTurn;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        public byte ProcessForward(byte input)
        {
            var result = _table.GetDirect(input);
            return result;
        }

        public byte ProcessBackwards(byte input)
        {
            return _table.GetReverse(input);
        }

        public virtual void Turn()
        {
            _table.Offset++;
            if (_table.Offset == 0)
                OnFullTurn();
        }
    }
}