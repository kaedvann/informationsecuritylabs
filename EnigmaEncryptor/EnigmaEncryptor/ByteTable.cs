﻿using System.Collections.Generic;
using System.Linq;

namespace EnigmaEncryptor
{
    internal class ByteTable
    {
        private readonly List<byte> _direct = new List<byte>(Enumerable.Range(0, 256).Select(i => (byte)i));

        public ByteTable(IList<byte> table)
        {
            
            for (int i = 0; i < 256; i++)
            {
                _direct[i] = table[i];
            }
        }

        public byte Offset { get; set; }

        public byte GetDirect(byte input)
        {
            return _direct[(byte)(Offset + input)];
        }

        public byte GetReverse(byte input)
        {
            return (byte) ((_direct.IndexOf(input)) - Offset);
        }
    }
}