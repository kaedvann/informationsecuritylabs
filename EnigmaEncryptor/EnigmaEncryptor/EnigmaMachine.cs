﻿using System;
using System.Collections.Generic;
using System.IO;

namespace EnigmaEncryptor
{
    public class EnigmaMachine: IEncryptor, IDecryptor
    {
        private readonly IList<Wheel> _wheels;

        public EnigmaMachine(IList<byte> firstWheelTable, IList<byte> secondWheelTable, IList<byte> thirdWheelTable,
            IList<byte> reflectorTable)
        {
            _wheels = new[]
            {
                Wheel.CreateWheel(firstWheelTable), Wheel.CreateWheel(secondWheelTable), Wheel.CreateWheel(thirdWheelTable),
                Reflector.CreateReflector(reflectorTable)
            };
            foreach (var wheel in _wheels)
            {
                wheel.FullTurn += WheelOnFullTurn;
            }

        }

        private void WheelOnFullTurn(object sender, EventArgs eventArgs)
        {
            var wheel = (Wheel) sender;
            int index = _wheels.IndexOf(wheel);
            if(index < 3)
                _wheels[index + 1].Turn();
        }

        private void Reset()
        {
            foreach (var wheel in _wheels)
            {
                wheel.Reset();
            }
        }

        private byte ProcessByte(byte item)
        {
            var result = _wheels[0].ProcessForward(item);
            result = _wheels[1].ProcessForward(result);
            result = _wheels[2].ProcessForward(result);
            result = _wheels[3].ProcessForward(result);
            result = _wheels[2].ProcessBackwards(result);
            result = _wheels[1].ProcessBackwards(result);
            result = _wheels[0].ProcessBackwards(result);
            _wheels[0].Turn();
            return result;
        }

        public void Encrypt(Stream input, Stream output)
        {
            int item;
            while ((item = input.ReadByte()) >= 0)
            {
                byte result = ProcessByte((byte)item);
                output.WriteByte(result);
            }
            output.Flush();
            output.Seek(0, SeekOrigin.Begin);
            Reset();
        }

        public void Decrypt(Stream input, Stream output)
        {
            Encrypt(input, output);
        }


    }
}
