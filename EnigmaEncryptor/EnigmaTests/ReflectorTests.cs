﻿using System;
using System.Collections.Generic;
using EnigmaEncryptor;
using NUnit.Framework;

namespace EnigmaTests
{
    [TestFixture]
    public class ReflectorTests
    {
        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void TestCreatingWithIncorrectTable()
        {
            IList<byte> table = new List<byte>{4,2,1,7,5,4,8,3,6};
            Reflector.CreateReflector(table);
        }
    }
}