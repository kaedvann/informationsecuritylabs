﻿using System;
using System.IO;
using System.Linq;
using EnigmaEncryptor;
using NUnit.Framework;

namespace EnigmaTests
{
    [TestFixture]
    public class EncryptorTests
    {
        [Test]
        public void SymmetricityTest()
        {
            var testBytes = new byte[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100, 99, 98, 97, 96, 95, 94, 93, 92, 91};
            var testStream = new MemoryStream(testBytes);
            var resultStream = new MemoryStream();
            var decryptedResult = new MemoryStream();
            var encryptor = new EnigmaMachine(PresetTables.FirstWheel, PresetTables.SecondWheel, PresetTables.ThirdWheel, PresetTables.Reflector);
            encryptor.Encrypt(testStream, resultStream);
            encryptor.Decrypt(resultStream, decryptedResult);
            using (var memoryStream = new MemoryStream())
            {
                decryptedResult.CopyTo(memoryStream);
                byte[] second = memoryStream.ToArray();
                Assert.True(testBytes.SequenceEqual(second));
            }
        }
    }
}
