﻿using System;
using System.Collections.Generic;
using System.IO;
using EnigmaEncryptor;

namespace SimpleEncryptor
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using(var inputFile = File.Open(args[0], FileMode.Open, FileAccess.Read))
                using (var outputFile = File.Open(args[1], FileMode.Create))
                {
                    EnigmaMachine machine = new EnigmaMachine(PresetTables.FirstWheel, PresetTables.SecondWheel, PresetTables.ThirdWheel, PresetTables.Reflector);
                    machine.Encrypt(inputFile, outputFile);
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine("Incorrect file paths");
            }
        }

        private static void GenerateWheels()
        {
            var reflector = TableGenerator.GenerateReflectorTable();
            var first = TableGenerator.GenereteWheelTable();
            var second = TableGenerator.GenereteWheelTable();
            var third = TableGenerator.GenereteWheelTable();
            using (var f = File.Open("table.txt", FileMode.Create))
            {
                WriteTable(f, reflector);
                WriteTable(f, first);
                WriteTable(f, second);
                WriteTable(f, third);
                f.Flush();
            }
        }

        private static void WriteTable(Stream stream, IEnumerable<byte> table)
        {
            stream.Seek(0, SeekOrigin.End);
            var writer = new StreamWriter(stream);
            writer.Write('{');
            foreach (var b in table)
            {
                writer.Write(b + ", ");
            }
            writer.Write('}');
            writer.WriteLine();
            writer.Flush();
        }
    }
}
